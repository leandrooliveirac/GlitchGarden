# GlitchGarden
A lane-based Tower Defense game developed with C# and Unity for learning purposes:

* Game Design
* Splash Screen
* Scaling & Aspect Ratios
* Background Playspace
* World Space Canvas Mode
* Basic Animation: Animator Controller & Animation
* Coroutines
* Animation Events
* Game UI
* PlayerPrefs To Save Settings

## Game Features:
* Different tower and enemy types
* Basic resources system


## How To Build / Compile
This is a Unity project, therefore it requires Unity3D to build. Clone or download this repo, and navigate to `Assets > Scenes` then open any `.unity` file.
